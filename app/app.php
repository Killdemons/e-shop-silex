<?php
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__."/../Db/User.php";
require_once __DIR__."/../Db/Products.php";
require_once __DIR__."/../Db/Checkout.php";
require_once __DIR__."/../Db/Categories.php";
require_once __DIR__."/../Db/Cart.php";

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

Debug::enable();
$app = new Silex\Application();
$app['debug'] = true;

session_start();
if(empty($_SESSION['id']) && empty($_SESSION['name']) && empty($_SESSION['type'])) {
    $_SESSION['id'] = null;
    $_SESSION['name'] = null;
    $_SESSION['type'] = null;
}

//Configuracion de la zona horaria
date_default_timezone_set("America/Costa_Rica");

//Registro de la BD
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_pgsql',
        'dbname'   => 'eshop',
        'host'   => 'localhost',
        'user'   => 'postgres',
        'password'   => 'bryan2748245',
        'port'   => 5432,
    ),
));

//Registro de las vistas
$app->register(new Silex\Provider\TwigServiceProvider(), array('twig.path' => __DIR__.'/../views'));


//Registro de los assets
$app->register(new Silex\Provider\AssetServiceProvider(), array(
    'assets.base_path' => ''
));

//Seccion pagina principal
$app->get('/', function () use ($app) {
    if ($_SESSION['type'] == null) {
        return $app['twig']->render('segurity/login.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"]));
    }
    elseif ($_SESSION['type'] == true) {
        return $app->redirect('/home_admin');
    }
    elseif ($_SESSION['type'] == false) {
        return $app->redirect('/home');
    }
});

//Seccion Signup
$app->get('/signup', function () use ($app) {
    return $app['twig']->render('segurity/signup.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"]));
});

//Seccion Login
$app->post('/login', function (Request $request) use ($app) {
    $email 	  = $request->get('email');
    $password = $request->get('password');
	$user = User::login($email, $password, $app);
    if ($user != null) {
    	$_SESSION['id'] = $user['id'];
        $_SESSION['name'] = $user['name'];
        $_SESSION['type'] = $user['type'];
        if ($_SESSION['type'] == 't') {
            return $app->redirect('/home_admin');
        }
        else {
            return $app->redirect('/home');
        }
    }else{
        $validacion = "<div class='alert alert-danger' role='alert'>Los datos ingresados no son correctos.</div>";
        return $app['twig']->render('segurity/login.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'validacion' => $validacion));
    }
});

//Seccion resgistro
$app->post('/signin', function (Request $request) use ($app) {
    $email = $request->get('email');
    $name = $request->get('name');
    $phone = $request->get('phone');
    $address = $request->get('address');
    $password = $request->get('password');
    $password_verification = $request->get('password_verification');
    $validar = User::validation($email, $app);
    if (!$validar == null || !empty($validar)) {
        $validacion = "<div class='alert alert-danger' role='alert'>Email digitado ya se encuentra registrado.</div>";
        return $app['twig']->render('segurity/signup.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'validacion' => $validacion));
    }
    else if ($password == $password_verification) {
        User::insert($email, $password, $name, $phone, $address, $app);
        $user = User::login($email, $password, $app);
        if ($user != false) {
            $_SESSION['id'] = $user['id'];
            $_SESSION['name'] = $user['name'];
            $_SESSION['type'] = $user['type'];
            return $app->redirect('/home');
        }
    }else {
        $validacion = "<div class='alert alert-danger' role='alert'>Contraseñas no coinciden.</div>";
        return $app['twig']->render('segurity/signup.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'validacion' => $validacion));
    }
});

//Seccion ADMIN
$app->get('/home_admin', function (Request $request) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    $usuario = User::count($app);
    $producto = Products::count($app);
    $total = Checkout::total($app);
    return $app['twig']->render('home/admin.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'usuario' => $usuario['count'], 'producto' => $producto['count'], 'total' => $total['sum']));
});

//Seccion Categorias
$app->get('/categories', function (Request $request) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    $categorias = Categories::select($app);
    return $app['twig']->render('Categories/index.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'categorias' => $categorias));
});

$app->get('/new_categorie', function (Request $request) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    $categorias = Categories::select($app);
    return $app['twig']->render('Categories/create.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'categorias' => $categorias));
});

$app->post('/add_categorie', function (Request $request) use ($app) {
    $name = $request->get('name');
    $name_sub = $request->get('name_sub');
    $validar = Categories::validation($name, $app);
    if (!$validar == null || !empty($validar)) {
        $categorias = Categories::select($app);
        $validacion = "<div class='alert alert-danger' role='alert'>Ese categoria ya esta registrado. Favor verificar que no es la misma categoria o cambie el nombre.</div>";
        return $app['twig']->render('Categories/create.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'categorias' => $categorias, 'validacion' => $validacion));
    }else{
        Categories::insert($name, $name_sub, $app);
        return $app->redirect('/categories');
    }
});

$app->get('/upd_categories/{id_cat}', function (Request $request, $id_cat) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    $find = Categories::find($id_cat, $app);
    $categorias = Categories::select($app);
    return $app['twig']->render('Categories/update.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'categorias' => $categorias, 'find' => $find));
});

$app->post('/upd_categories', function (Request $request) use ($app) {
    $id = $request->get('id');
    $name = $request->get('name');
    $name_sub = $request->get('name_sub');
    Categories::update($id, $name, $name_sub, $app);
    return $app->redirect('/categories');
});

$app->get('/del_categories/{id_cat}', function (Request $request, $id_cat) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    Categories::delete($id_cat, $app);
    return $app->redirect('/categories');
});
//Fin seccion categorias

//Seccion Productos
$app->get('/products', function (Request $request) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    $productos = Products::select($app);
    return $app['twig']->render('Products/index.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'productos' => $productos));
});

$app->get('/new_product', function (Request $request) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    $categorias = Categories::select($app);
    return $app['twig']->render('Products/create.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'categorias' => $categorias));
});

$app->post('/add_product', function (Request $request) use ($app) {
    $sku = $request->get('sku');
    $name = $request->get('name');
    $description = $request->get('description');
    $imagen = $request->get('imagen');
    $categorie = $request->get('categorie');
    $stock = $request->get('stock');
    $price = $request->get('price');
    $validar = Products::validation($sku, $app);
    if (!$validar == null || !empty($validar)) {
        $categorias = Categories::select($app);
        $validacion = "<div class='alert alert-danger' role='alert'>Ese SKU ya esta registrado. Favor verificar que no es el mismo producto o cambie el SKU.</div>";
        return $app['twig']->render('Products/create.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'categorias' => $categorias, 'validacion' => $validacion));
    }else{
        Products::insert($sku, $name, $description, $imagen, $categorie, $stock, $price, $app);
        return $app->redirect('/products');
    }
});

$app->get('/upd_products/{id_prod}', function (Request $request, $id_prod) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    $find = Products::find($id_prod, $app);
    $categorias = Categories::select($app);
    return $app['twig']->render('Products/update.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'categorias' => $categorias, 'find' => $find));
});

$app->post('/upd_products', function (Request $request) use ($app) {
    $id = $request->get('id');
    $sku = $request->get('sku');
    $name = $request->get('name');
    $description = $request->get('description');
    $imagen = $request->get('imagen');
    $categorie = $request->get('categorie');
    $stock = $request->get('stock');
    $price = $request->get('price');
    Products::update($id, $sku, $name, $description, $imagen, $categorie, $stock, $price, $app);
    return $app->redirect('/products');
});

$app->get('/del_products/{sku_prod}', function (Request $request, $sku_prod) use ($app) {
    if ($_SESSION['type'] != 't') {
        return $app->redirect('/access_denied/guard_admin');
    }
    Products::delete($sku_prod, $app);
    return $app->redirect('/products');
});
//Fin seccion productos
//Fin seccion ADMIN

//Seccion USER
$app->get('/home', function (Request $request) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    $producto = Checkout::count($_SESSION['id'], $app);
    return $app['twig']->render('home/user.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'producto' => $producto));
});

$app->get('/catalog', function (Request $request) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    $catalogo = Products::select($app);
    $categories = Categories::select($app);
    return $app['twig']->render('Catalog/index.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'catalogo' => $catalogo, 'categories' => $categories));
});

$app->get('/catalog/{categoria}/{id_prod}', function (Request $request, $categoria, $id_prod) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    elseif ($id_prod == '0') {
        $catalogo = Products::find_sub($categoria, $app);
    }
    else{
        $catalogo = Products::find_cate($categoria, $app);
    }
    $categories = Categories::select($app);
    return $app['twig']->render('Catalog/index.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'catalogo' => $catalogo, 'categories' => $categories, 'categoria' => $categoria, 'id_prod' => $id_prod));
});

$app->get('/add_cart/{id}/{categoria}/{id_prod}', function (Request $request, $id, $categoria, $id_prod) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    $producto = Products::find($id, $app);
    Cart::insert($_SESSION['id'], $producto['sku'], $producto['name'], $producto['description'], $producto['imagen'], $producto['price'], $app);
    if ($categoria == 'null' && $id_prod == 'null') {
        return $app->redirect('/catalog');
    }else{
        return $app->redirect('/catalog/' . $categoria . '/' . $id_prod);
    }
});

$app->get('/cart', function (Request $request) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    $carrito = Cart::find($_SESSION['id'], $app);
    return $app['twig']->render('Cart/index.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'carrito' => $carrito));
});

$app->get('/del_cart/{id}', function (Request $request, $id) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    Cart::delete($id, $app);
    return $app->redirect('/cart');
});

$app->get('/checkout', function (Request $request) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    $carrito = Cart::find($_SESSION['id'], $app);
    return $app['twig']->render('Cart/checkout.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'carrito' => $carrito));
});

$app->post('/buy', function (Request $request) use ($app) {
    $total = 0;
    $carrito = Cart::find($_SESSION['id'], $app);
    $fecha = date('Y/m/d H:i');
    $id_orden = $_SESSION['name'] . date('dsu');
    foreach ($carrito as $producto) {
        Checkout::insert_orden($id_orden, $producto['sku'], $producto['name'], $producto['description'], $producto['imagen'], $producto['price'], $app);
        Products::update_stock($producto['sku'], 1, $app);
        Cart::delete($producto['id'], $app);
        $total += $producto['price'];
    }
    Checkout::insert_checkout($_SESSION['id'], $fecha, $id_orden, $total, $app);
    return $app->redirect('/cart');
});

$app->get('/record', function (Request $request) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    $checkout = Checkout::find_checkout($_SESSION['id'], $app);
    return $app['twig']->render('Record/index.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'checkout' => $checkout));
});

$app->get('/details/{id}', function (Request $request, $id) use ($app) {
    if (!isset($_SESSION['id']) || $_SESSION['id'] == null) {
        return $app->redirect('/access_denied/guard');
    }
    $detalles = Checkout::details($id, $app);
    return $app['twig']->render('Record/details.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'detalles' => $detalles));
});

//Fin seccion USER

$app->get('/logout', function (Request $request) use ($app) {
    session_destroy();
    return $app->redirect('/');
});

//Cuando sucede una excepcion o error
$app->get('/access_denied/{access}', function (Request $request, $access) use ($app) {
    return $app['twig']->render('segurity/guard.html.twig', array('alert' => null, 'id' => $_SESSION['id'], 'name' => $_SESSION['name'], 'type' => $_SESSION['type'], 'CurrentUrl' => $_SERVER["REQUEST_URI"], 'access' => $access));
});

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    return $app->redirect('/access_denied/' . $code);
});
    
return $app;