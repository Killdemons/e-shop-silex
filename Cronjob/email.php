<?php
require '../vendor/autoload.php';
require_once __DIR__."/../Db/Products.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$app = new Silex\Application();

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_pgsql',
        'dbname'   => 'eshop',
        'host'   => 'localhost',
        'user'   => 'postgres',
        'password'   => 'bryan2748245',
        'port'   => 5432,
    ),
));
$cantidad = $argv[1];
$productos = Products::cronjob($cantidad, $app);
$mail = new PHPMailer(true);
try {
    //Server settings
    $mail->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'kore281295@gmail.com';
    $mail->Password = 'bryan2748245';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    //Recipients
    $mail->setFrom('eshop@eshop.com', 'E-Shop');
    $mail->addAddress('kore281295@gmail.com', 'Bryan Campos'); 

    //Content
    $mail->isHTML(true);
    $mail->Subject = 'Productos bajos en stock';
    $num = 1;
    foreach ($productos as $producto) {
    	$mail->Body .= '<h5>Producto ' . $num++ . '</h5>' .
    	'SKU: ' . $producto['sku'] .
    	'<br>Nombre: ' . $producto['name'] .
    	'<br>Descripcion: ' . $producto['description'] .
    	'<br>Categoria: ' . $producto['categorie']
    ;}
    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

$app->run();