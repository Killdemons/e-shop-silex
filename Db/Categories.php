<?php
    class Categories
    {
        public function select($app)
        {
            $sql = "SELECT * FROM categories ORDER BY id";
            $post = $app['db']->fetchAll($sql, array());
            return $post;
        }

        public function insert($name, $name_sub, $app)
        {
    		$app['db']->insert('categories', array('name' => $name, 'name_sub' => $name_sub));
        }

        public function validation($name, $app)
        {
            $sql = "SELECT * FROM categories WHERE name = ?";
            $post = $app['db']->fetchAssoc($sql, array($name));
            return $post;
        }

        public function find($id, $app)
        {
            $sql = "SELECT * FROM categories WHERE id = ?";
            $post = $app['db']->fetchAssoc($sql, array($id));
            return $post;
        }

        public function update($id, $name, $name_sub, $app)
        {
            $app['db']->update('categories', array('name' => $name, 'name_sub' => $name_sub), array('id' => $id));
        }

        public function delete($id, $app)
        {
            $app['db']->delete('categories', array('id' => $id));
        }
    }
?>