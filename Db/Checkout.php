<?php
    class Checkout
    {
        public function total($app)
        {
            $sql = "SELECT SUM(price) FROM orden";
            $post = $app['db']->fetchAssoc($sql, array());
            return $post;
        }

        public function count($id, $app)
        {
            $sql = "SELECT COUNT(o.id_orden), SUM(o.price) FROM orden o, checkout c WHERE c.id_user = ? AND o.id_orden = c.id_orden";
            $post = $app['db']->fetchAssoc($sql, array($id));
            return $post;
        }

        public function insert_orden($id_orden, $sku, $name, $description, $imagen, $price, $app)
        {
            $app['db']->insert('orden', array('id_orden' => $id_orden, 'sku' => $sku, 'name' => $name, 'description' => $description, 'imagen' => $imagen, 'price' => $price));
        }

        public function insert_checkout($id_user, $fecha, $id_orden, $total, $app)
        {
            $app['db']->insert('checkout', array('id_user' => $id_user, 'date' => $fecha, 'id_orden' => $id_orden, 'total' => $total));
        }

        public function find_checkout($id, $app)
        {
            $sql = "SELECT * FROM checkout WHERE id_user = ?";
            $post = $app['db']->fetchAll($sql, array($id));
            return $post;
        }

        public function details($id, $app)
        {
            $sql = "SELECT * FROM orden WHERE id_orden = ?";
            $post = $app['db']->fetchAll($sql, array($id));
            return $post;
        }
    }
?>
