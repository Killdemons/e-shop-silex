<?php
    class Products
    {
        public function count($app)
        {
            $sql = "SELECT COUNT(*) FROM products";
            $post = $app['db']->fetchAssoc($sql, array());
            return $post;
        }

        public function select($app)
        {
            $sql = "SELECT * FROM products ORDER BY id";
            $post = $app['db']->fetchAll($sql, array());
            return $post;
        }

        public function insert($sku, $name, $description, $imagen, $categorie, $stock, $price, $app)
        {
    		$app['db']->insert('products', array('sku' => $sku, 'name' => $name, 'description' => $description, 'imagen' => $imagen, 'categorie' => $categorie, 'stock' => $stock, 'price' => $price));
        }

        public function validation($sku, $app)
        {
            $sql = "SELECT * FROM products WHERE sku = ?";
            $post = $app['db']->fetchAssoc($sql, array($sku));
            return $post;
        }

        public function find($id, $app)
        {
            $sql = "SELECT * FROM products WHERE id = ?";
            $post = $app['db']->fetchAssoc($sql, array($id));
            return $post;
        }

        public function find_cate($categorie, $app)
        {
            $sql = "SELECT * FROM products WHERE categorie = ? ORDER BY id";
            $post = $app['db']->fetchAll($sql, array($categorie));
            return $post;
        }

        public function find_sub($subcategorie, $app)
        {
            $post = [];
            $sql = "SELECT * FROM categories WHERE name_sub = ? ORDER BY id";
            $subcategories = $app['db']->fetchAll($sql, array($subcategorie));
            $sqls = "SELECT * FROM products WHERE categorie = ? ORDER BY id";
            foreach ($subcategories as $categorie) {
                $post = array_merge($post, $app['db']->fetchAll($sqls, array($categorie['name'])));
            }
            
            return $post;
        }

        public function update($id, $sku, $name, $description, $imagen, $categorie, $stock, $price, $app)
        {
            $app['db']->update('products', array('sku' => $sku, 'name' => $name, 'description' => $description, 'imagen' => $imagen, 'categorie' => $categorie, 'stock' => $stock, 'price' => $price), array('id' => $id));
        }

        public function delete($sku, $app)
        {
            $app['db']->delete('products', array('sku' => $sku));
        }

        public function update_stock($sku, $stock, $app)
        {
            $sql = "SELECT stock FROM products WHERE sku = ?";
            $post = $app['db']->fetchAssoc($sql, array($sku));
            $app['db']->update('products', array('stock' => ((int)$post['stock']-$stock)), array('sku' => $sku));
        }

        public function cronjob($cantidad, $app)
        {
            $sql = "SELECT * FROM products WHERE stock < ? ORDER BY id";
            $post = $app['db']->fetchAll($sql, array((int)$cantidad));
            return $post;
        }
    }
?>
