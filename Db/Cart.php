<?php
    class Cart
    {
        public function insert($id_client, $sku, $name, $description, $imagen, $price, $app)
        {
    		$app['db']->insert('cart', array('id_client' => $id_client, 'sku' => $sku, 'name' => $name, 'description' => $description, 'imagen' => $imagen, 'price' => $price));
        }

        public function find($id, $app)
        {
            $sql = "SELECT * FROM cart WHERE id_client = ? ORDER BY sku";
            $post = $app['db']->fetchAll($sql, array($id));
            return $post;
        }

        public function delete($id, $app)
        {
            $app['db']->delete('cart', array('id' => $id));
        }
    }
?>