<?php
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/Email.php';

use Symfony\Component\Console\Application;

$app = new Application();

$app->add(new Consola\Email());

$app->run();