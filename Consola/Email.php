<?php

namespace Consola;

require_once __DIR__."/../Db/Products.php";

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Email extends Command
{
    protected function configure()
    {
        $this
            ->setName('Cantidad')
            ->setDescription('Revision del stock de productos')
            ->addArgument(
                'cantidad',
                InputArgument::OPTIONAL,
                'Cantidad minima para validar el stock de los productos?'
            )
            ->addOption(
               'yell',
               null,
               InputOption::VALUE_NONE,
               'If set, the task will yell in uppercase letters'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cantidad = $input->getArgument('cantidad');
        if (is_numeric($cantidad)) {
            $productos = Products::cronjob($cantidad, $app);
            $mail = new PHPMailer(true);
            try {
                //Server settings
                $mail->SMTPDebug = 2;
                $mail->isSMTP();
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'kore281295@gmail.com';
                $mail->Password = 'bryan2748245';
                $mail->SMTPSecure = 'tls';
                $mail->Port = 587;

                //Recipients
                $mail->setFrom('eshop@eshop.com', 'E-Shop');
                $mail->addAddress('kore281295@gmail.com', 'Bryan Campos'); 

                //Content
                $mail->isHTML(true);
                $mail->Subject = 'Productos bajos en stock';
                $num = 1;
                foreach ($productos as $producto) {
                    $mail->Body .= '<h5>Producto ' . $num++ . '</h5>' .
                    'SKU: ' . $producto['sku'] .
                    '<br>Nombre: ' . $producto['name'] .
                    '<br>Descripcion: ' . $producto['description'] .
                    '<br>Categoria: ' . $producto['categorie']
                ;}
                $mail->send();
                $text = 'Message has been sent';
            } catch (Exception $e) {
                $text = 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;
            }
        } else {
            $text = 'Favor poner una cantidad';
        }

        if ($input->getOption('yell')) {
            $text = strtoupper($text);
        }

        $output->writeln($text);
    }
}